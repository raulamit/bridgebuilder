package webapp.api.joshua;

import webapp.api.ConnectionManager;
import webapp.api.joshua.JoshuaCountry;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// data layer object to read and write Joshua countries into and out of SQL
public class JoshuaCountryDAO extends ConnectionManager {
	
	
	
	public JoshuaCountryDAO(String connectionString) {
		super(connectionString);
		// TODO Auto-generated constructor stub
	}

	// Use this one until the url is not hardcoded
	public JoshuaCountryDAO() {
		// TODO Auto-generated constructor stub
	}

	public void insert(JoshuaCountry country) {
		String sql = "INSERT INTO joshua_country" + 
				"(country_code, name, lang_code, official_lang, jp_scale, pop_total, pop_unreached, percent_unreached, percent_urbanized, percent_literate, percent_evangelical, primary_religion) " + 
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, country.getCountryCode());
			ps.setString(2, country.getName());
			ps.setString(3, country.getLanguageCode());
			ps.setString(4, country.getOfficialLanguage());
			ps.setShort(5, country.getJpScale());
			ps.setLong(6, country.getPopulation());
			ps.setLong(7, country.getPopulationUnreached());
			ps.setShort(8, country.getPercentUnreached());
			ps.setShort(9, country.getPercentUrbanized());
			ps.setShort(10, country.getPercentLiterate());
			ps.setShort(11, country.getPercentEvangelical());
			ps.setString(12, country.getPrimaryReligion());
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	// @brief: constructs a JoshuaCountry from any result set
	private JoshuaCountry JoshuaCountryFactory(ResultSet rs) throws SQLException {
		JoshuaCountry country = new JoshuaCountry();
		
		// populate all the set methods
		country.setCountryCode(rs.getString("country_code"));
		country.setName(rs.getString("name"));
		country.setLanguageCode(rs.getString("lang_code"));
		country.setOfficialLanguage(rs.getString("official_lang"));
		country.setJpScale(rs.getShort("jp_scale"));
		country.setPopulation(rs.getLong("pop_total"));
		country.setPopulationUnreached(rs.getLong("pop_unreached"));
		country.setPercentUnreached(rs.getShort("percent_unreached"));
		country.setPercentUrbanized(rs.getShort("percent_urbanized"));
		country.setPercentLiterate(rs.getShort("percent_literate"));
		country.setPercentEvangelical(rs.getShort("percent_evangelical"));
		country.setPrimaryReligion(rs.getString("primary_religion"));
		
		return country;
	}
	
	public List<JoshuaCountry> findAllCountries() {
		String sql = "SELECT * FROM joshua_country";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			PreparedStatement ps = conn.prepareStatement(sql);
			List<JoshuaCountry> countries = new ArrayList<JoshuaCountry>();
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				countries.add(JoshuaCountryFactory(rs));
			}
			rs.close();
			ps.close();
			return countries;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	}
	
	public JoshuaCountry findByCountryCode(String countryCode) {
		String sql = "SELECT * FROM joshua_country WHERE country_code = ?";
		
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, countryCode);
			JoshuaCountry country = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				country =  JoshuaCountryFactory(rs);
			}
			rs.close();
			ps.close();
			return country;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	}

}
