package webapp;

import java.util.*;
import javax.servlet.http.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

//import com.careerjet.webservice.api.Client;
//import com.google.gson.Gson;
//import webapp.api.career.CareerData;
//import webapp.api.joshua.JoshuaCountry;

@SpringBootApplication
public class Application {
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder){
		return builder.build();
	}
	
//	@Bean
//	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
//		log.debug("COMMAND LINE RUNNER");
//		return args -> {
//			
////			JoshuaCountry country = restTemplate.getForObject("https://joshuaproject.net/api/v2/countries?api_key=7B08wuY152TI", JoshuaCountry.class);
//			
//		};
//	}
	
	
	// Runs at startup
	// TODO complete API calls and database storage when the server starts
	// so that the information can be retrieved quickly
	/*@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		log.debug("COMMAND LINE RUNNER");
		return args -> {
			JoshuaCountry country = restTemplate.getForObject("https://joshuaproject.net/api/v2/countries?api_key=7B08wuY152TI", JoshuaCountry.class);
			log.info(country.toString());
		};
	}*/
}

	
	//get json object from careerjet
	public JSONObject getData(String countryCode, String keywords, HttpServletRequest request) {
		   Client c = new Client("en_GB");

		   Map<String, String> map = new HashMap<String, String>();
		   
		   //mandatory keys
		   map.put("affid", "285def1aa819b8b7211f291d2b5f8bf0");
		   map.put("user_ip",    request.getRemoteAddr());
		   map.put("user_agent", request.getHeader("User-Agent"));
		   map.put("url",        request.getRequestURL().toString());

		   //available keys
		   map.put("keywords", keywords);
		   map.put("location", countryCode);
		   
		   JSONObject results = (JSONObject) c.search(map);

		   return results;
	}
	
	//convert json to CareerData 
	public CareerData JSONToCareerData(JSONObject obj){
		Gson gson = new Gson();
		CareerData careerObj = gson.fromJson(obj.toString(), CareerData.class);
		return careerObj;		
	}
