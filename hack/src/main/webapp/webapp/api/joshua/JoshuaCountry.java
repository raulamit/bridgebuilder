package webapp.api.joshua;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class JoshuaCountry {
	
	// Population								// Api equivalent
	private long population;					// Population
	
	// Country facts
	private String name;						// Ctry
	private String countryCode;					// ROG3
	private String officialLanguage;			// OfficialLang
	private String languageCode;				// ROL3OfficialLanguage
	private short percentUrbanized;				// PercentUrbanized
	private short percentLiterate;				// LiteracyRate
	
	// Religious
	private long populationUnreached;			// PoplPeoplesLR
	private short percentUnreached;				// calculated
	private short percentEvangelical;			// PercentEvangelical
	private String primaryReligion;				// ReligionPrimary
	
	// JP rankings
	private short jpScale;						// JPScaleCtry
	
	
	public JoshuaCountry(){
		
	}
	
	public short getPercentLiterate() {
		return percentLiterate;
	}
	public void setPercentLiterate(short percentLiterate) {
		this.percentLiterate = percentLiterate;
	}
	
	public short getPercentUrbanized() {
		return percentUrbanized;
	}
	public void setPercentUrbanized(short percentUrbanized) {
		this.percentUrbanized = percentUrbanized;
	}
	
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String code) {
		this.languageCode = code;
	}
	
	public short getPercentEvangelical() {
		return percentEvangelical;
	}
	public void setPercentEvangelical(short percentEvangelical) {
		this.percentEvangelical = percentEvangelical;
	}
	
	public short getJpScale() {
		return jpScale;
	}
	public void setJpScale(short jpScale) {
		this.jpScale = jpScale;
	}

	public long getPopulation() {
		return population;
	}

	public void setPopulation(long population) {
		this.population = population;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getOfficialLanguage() {
		return officialLanguage;
	}

	public void setOfficialLanguage(String officialLanguage) {
		this.officialLanguage = officialLanguage;
	}

	public long getPopulationUnreached() {
		return populationUnreached;
	}

	public void setPopulationUnreached(long populationUnreached) {
		this.populationUnreached = populationUnreached;
	}

	public short getPercentUnreached() {
		return percentUnreached;
	}

	public void setPercentUnreached(short percentUnreached) {
		this.percentUnreached = percentUnreached;
	}

	public String getPrimaryReligion() {
		return primaryReligion;
	}

	public void setPrimaryReligion(String primaryReligion) {
		this.primaryReligion = primaryReligion;
	}
	
	@Override
	public String toString(){ // TODO Include percent unreached?
		return "Country{" +
				"name=" + name +
				" countryCode=" + countryCode +
				" officialLanguage=" + officialLanguage +
				" populationUnreached=" + populationUnreached + 
				" primaryReligion=" + primaryReligion +
				"}";
	}
	
	public static String[] getHeaders(){
		String[] array = { "name", "countryCode", "officialLanguage", "languageCode", "percentUrbanized",
				"percentLiterate", "populationUnreached", "percentUnreached", "percentEvangelical", "primaryReligion",
				"jpScale"};
		return array;
	}
	
	public String[] getValues(){
		String[] array = { getName(), getCountryCode(), getOfficialLanguage(), getLanguageCode(),
				"" + getPercentUrbanized(), "" + getPercentLiterate(), "" + getPopulationUnreached(),
				"" + getPercentUnreached(), "" + getPercentEvangelical(), getPrimaryReligion(), "" + getJpScale()
		};
		return array;
	}
}
