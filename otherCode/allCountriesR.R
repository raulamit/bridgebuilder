#Normalizing function
normalize_stuff <- function(x) {
xnorm = (x - min(x))/(max(x)-min(x))
         return(xnorm)
}

read.csv("allCountries.csv",header=TRUE)

hist(allCountries$PercentChristianity) #Not normal
allCountries$PercentChristianity = allCountries$PercentChristianity/100
#First transform PercentChristianity value
allCountries$PercentChristainityLogit = logit(allCountries$PercentChristianity)
hist(allCountries$PercentChristainityLogit) #normal-ish
#Then normalize
allCountries$PercentChristianityNorm = normalize_stuff(allCountries$PercentChristianityLogit)

hist(allCountries$PoplPeoples) #Not very normal
#allCountries$PoplPeoplesLog = log(allCountries$PoplPeoples)
allCountries$PoplPeoplesPower=(allCountries$PoplPeoples)^0.1
hist(allCountries$PoplPeoplesPower) #normal-ish
allCountries$PoplPeoplesNorm = normalize_stuff(allCountries$PoplPeoplesLog) #normalize

#Make smallSet

smallSet = allCountries[allCountries$Ctry %in% c('Yemen','Somalia','Eritrea','Afghanistan','Uzbekistan','Pakistan','Djibouti','Turkey','Saudi Arabia','Tajikistan'),]

smallSet$score = c(90.9,90,88.5,88.3,88,87.3,87,87,86.8,86.7)

#smallSet$X10_40Window = as.factor(smallSet$X10_40Window)

#Try a linear model based on some factors that might predict the degree of need for a country
#http://www.peoplegroups.org/Documents/26-6-joshua-project.pdf
smallsetlm = lm(smallSet$score~smallSet$JPScaleCtry+smallSet$PoplPeoples+smallSet$PercentEvangelical)

smallsetlm

summary(smallsetlm)

#Coefficients:
#  (Intercept)         smallSet$JPScaleCtry         smallSet$PoplPeoples  
#8.784e+01                    6.663e-01                   -1.508e-08  
#smallSet$PercentEvangelical  
#-9.087e-01

#Therefore equation is
#smallset = 0.8784 + 0.6663*JPScaleCtry - 1.508e08*PoplPeoples - 0.9087*PercentEvangelical

#Try another linear model with normalized values

smallsetlm2 = lm(smallSet$score~smallSet$JPScaleCtry+smallSet$PoplPeoplesNorm+smallSet$PercentChristianityNorm)
