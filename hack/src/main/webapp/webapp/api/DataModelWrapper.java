package webapp.api;

import java.util.ArrayList;
import java.util.Iterator;

public class DataModelWrapper {
	
	private ArrayList<DataModel> dataModelList;
	
	public DataModelWrapper() {
		dataModelList = new ArrayList<DataModel>();
	}
	
	public void addDataModel(DataModel model){
		dataModelList.add(model);
	}
	
	public ArrayList<DataModel> getDataModelList(){
		return dataModelList;
	}
	
	public String getCSV(){
		String headers = DataModel.getCSVHeaders();
		
		StringBuilder stringBuilder = new StringBuilder(headers);
		
		Iterator<DataModel> iterator = dataModelList.iterator();
		while (iterator.hasNext()){
			stringBuilder.append("\n");
			DataModel model = iterator.next();
			stringBuilder.append(model.getCSV());
		}
		
		return "";
	}
	
}
