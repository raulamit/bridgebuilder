package webapp.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CsvFile {
	private String csvPath;
	private String[] headers;
	private BufferedReader br;
	static private String splitString = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
	
	public CsvFile(String csvPath) {
		this.csvPath = csvPath;
		this.headers = null;
		br = null;
	}
	
	public void open() throws FileNotFoundException, IOException {
		this.br = new BufferedReader(new FileReader(this.csvPath));
		String line = "";
		line = this.br.readLine();
		if (line != null)
		{
			this.headers = line.split(splitString);
		}
	}
	
	public Map<String, String> readLine() throws IOException {
		Map<String, String> lineMap = null;
		
		String line = this.br.readLine();
		if (line != null) {
			String[] csvData = line.split(splitString);
			lineMap = new HashMap<String, String>();
			for (int i = 0; i < csvData.length; i++) {
				lineMap.put(headers[i], csvData[i]);
			}
		}
		
		return lineMap;
	}
	
	public void close() throws IOException {
		this.br.close();
	}
}
