package webapp.api;

import webapp.api.career.CareerData;
import webapp.api.economy.EconomyData;
import webapp.api.joshua.JoshuaCountry;

// Poor naming :(
public class DataModel {
	
	private JoshuaCountry country;
	private EconomyData economyData;
	private CareerData careerData;
	public JoshuaCountry getCountry() {
		return country;
	}
	public void setCountry(JoshuaCountry country) {
		this.country = country;
	}
	public EconomyData getEconomyData() {
		return economyData;
	}
	public void setEconomyData(EconomyData economyData) {
		this.economyData = economyData;
	}
	public CareerData getCareerData() {
		return careerData;
	}
	public void setCareerData(CareerData careerData) {
		this.careerData = careerData;
	}
	public static String getCSVHeaders() {
		// TODO Auto-generated method stub
		String[] countryHeaders = JoshuaCountry.getHeaders();
		String[] economyHeaders = EconomyData.getHeaders();
		String[] careerHeaders = CareerData.getHeaders();
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 0; i < countryHeaders.length; i++){
			stringBuilder.append(countryHeaders[i]);
			if (i + 1 < countryHeaders.length){
				stringBuilder.append(",");
			}
		}
		
		for (int i = 0; i < economyHeaders.length; i++){
			stringBuilder.append(",");
			stringBuilder.append(economyHeaders[i]);
		}
		
		for (int i = 0; i < careerHeaders.length; i++){
			stringBuilder.append(",");
			stringBuilder.append(careerHeaders[i]);
		}
		
		return stringBuilder.toString();
	}
	public String getCSV() {
		// TODO Auto-generated method stub
		String[] countryValues = country.getValues();
		String[] economyValues = economyData.getValues();
		String[] careerValues = careerData.getValues();
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 0; i < countryValues.length; i++){
			stringBuilder.append(countryValues[i]);
			if (i + 1 < countryValues.length){
				stringBuilder.append(",");
			}
		}
		
		for (int i = 0; i < economyValues.length; i++){
			stringBuilder.append(",");
			stringBuilder.append(economyValues[i]);
		}
		
		for (int i = 0; i < careerValues.length; i++){
			stringBuilder.append(",");
			stringBuilder.append(careerValues[i]);
		}
		
		return stringBuilder.toString();
	}
	
}
