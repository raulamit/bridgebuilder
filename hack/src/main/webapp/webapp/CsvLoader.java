package webapp;

import java.util.Map;

import webapp.api.joshua.JoshuaCountry;
import webapp.api.joshua.JoshuaCountryDAO;
import webapp.utils.CsvFile;

public class CsvLoader {
	public static float parseFloatDefault(String value, float defaultValue)
	{
		try {
			return Float.parseFloat(value);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public static void main(String[] args) {
		try {
			CsvFile csvFile = new CsvFile("C:\\Users\\rmest\\code\\jobs\\data-raw\\joshua_project\\allCountries.csv");
			csvFile.open();
			
			Map<String, String> line = null;
			JoshuaCountryDAO joshuaDao = new JoshuaCountryDAO();
			while ((line = csvFile.readLine()) != null) {
				JoshuaCountry country = new JoshuaCountry();
				country.setCountryCode(line.get("ROG3"));
				country.setName(line.get("Ctry"));
				country.setLanguageCode(line.get("ROL3OfficialLanguage"));
				country.setOfficialLanguage(line.get("OfficialLang"));
				country.setPrimaryReligion(line.get("ReligionPrimary"));
				country.setPopulation(Long.parseLong(line.get("Population")));
				
				country.setPercentEvangelical((short)Math.round(parseFloatDefault(line.get("PercentEvangelical"), 0.0F)));
				country.setPercentLiterate((short)Math.round(parseFloatDefault(line.get("LiteracyRate"), 0.0F)));
				country.setPercentUrbanized((short)Math.round(parseFloatDefault(line.get("PercentUrbanized"), 0.0F)));
				country.setJpScale(Short.parseShort(line.get("JPScaleCtry")));
				
				float percentUnreached = 1.0F - parseFloatDefault(line.get("PercentChristianity"), 0.0F) / 100.0F;
				country.setPercentUnreached((short)Math.round(percentUnreached * 100.0F));
				country.setPopulationUnreached(Math.round((float)country.getPopulation() * percentUnreached));
				System.out.println(country.toString());
				joshuaDao.insert(country);
			}
			csvFile.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception Failed." + e.toString());
		}
	}

}
