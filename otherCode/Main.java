import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

//Purpose of code is to show how to run rscript within java
public class Main{

public static void main(String[] args) throws Exception {

	Path currentRelativePath = Paths.get("");
	String currentPath = currentRelativePath.toAbsolutePath().toString();
	System.out.println(currentPath);
	Runtime.getRuntime().exec("Rscript "+currentPath+"/rscript.r"); 
	}
}
