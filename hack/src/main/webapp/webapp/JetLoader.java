package webapp;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.careerjet.webservice.api.Client;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import webapp.api.career.CareerDAO;
import webapp.api.career.CareerData;
import webapp.api.joshua.JoshuaCountry;
import webapp.api.joshua.JoshuaCountryDAO;
import webapp.utils.DateDeserializer;
import webapp.utils.FloatDeserializer;

public class JetLoader {
	
	//get json object from careerjet
	static public JSONObject getData(String countryCode, String keywords/*, HttpServletRequest request*/) {
		   Client c = new Client("en_GB");

		   Map<String, String> map = new HashMap<String, String>();
		   
		   //mandatory keys
		   map.put("affid", "285def1aa819b8b7211f291d2b5f8bf0");
		   map.put("user_ip",    "101.98.62.130");
		   map.put("user_agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36");
		   map.put("url",        "http://localhost:8080");
//		   map.put("user_ip",    request.getRemoteAddr());
//		   map.put("user_agent", request.getHeader("User-Agent"));
//		   map.put("url",        request.getRequestURL().toString());

		   //available keys
		   map.put("keywords", keywords);
		   map.put("location", countryCode);
		   
		   JSONObject results = (JSONObject) c.search(map);

		   return results;
	}
	
	//convert json to CareerData 
	static public CareerData JSONToCareerData(JSONObject obj){
		System.out.println(obj.toString());
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		gsonBuilder.registerTypeAdapter(Float.class, new FloatDeserializer());
		Gson gson = gsonBuilder.create();
		CareerData careerObj = gson.fromJson(obj.toString(), CareerData.class);
		return careerObj;		
	}
	
	public static void main(String[] args) {
		JoshuaCountryDAO joshuaDoa = new JoshuaCountryDAO();
		
		List<JoshuaCountry> countries = joshuaDoa.findAllCountries();
		
		CareerDAO careerDao = new CareerDAO();
		
		for (JoshuaCountry country : countries) {
			CareerData careerData = JSONToCareerData(getData(country.getCountryCode(), ""));
			careerData.setCountryCode(country.getCountryCode());
			careerDao.insert(careerData);
			System.out.println(careerData.toString());
		}
	}
}
