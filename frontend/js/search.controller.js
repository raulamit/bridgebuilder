angular.module('jobfinder')
  .controller('SearchController', function ($scope, $log, SearchService) {
    
    $scope.selectedIndustry = 'Select an industry that excites you';

    $scope.industries = [
      'Accommodation and Hospitality',
      'Agriculture, horticulture, and forestry',
      'Healthcare',
      'Construction',
      'Energy',
      'Education',
      'Film and Television',
      'Information communications and technology',
      'Manufacturing and production',
      'Retail trade',
      'Tourism',
      'Wholesale trade'
    ];

    $scope.searchservice = SearchService;
    

    $scope.status = {
      isopen: false
    };

    $scope.toggled = function(open) {
      
    };

    $scope.change = function(name){
      $scope.selectedIndustry = name;
      SearchService.search(name);
    }

    $scope.showinfo = function(result){
      
    }

    $scope.toggleDropdown = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.isopen = !$scope.status.isopen;
      
    };

    $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));

    $('.job-card__inner').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3
    });


  });